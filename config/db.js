const mongoose = require("mongoose")

const config = require("config")

//grab string inside config
const db = config.get("mongoURI")


const connectDB = async () => {
    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            // useFindAndModify: false,
            useCreateIndex: true
        })
        console.log("Mongo Atlas Connected")
    } catch (e) {
        console.error(err.message)
        process.exit(1)
    }
}


module.exports = connectDB;