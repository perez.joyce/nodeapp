const Task = require('../models/tasks');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

//CREATE A TASK
router.post('/', auth, async (req, res) => {
  res.send(req.body)
  // const task = new Task({
  //   //use SPREAD OPERATOR to copy body to new task
  //   ...req.body,
  //   //add memberId property
  //   memberId: req.member._id
  // });

  // try {
  //   await task.save();
  //   //CREATED
  //   res.status(201).send(task);
  // } catch (e) {
  //   res.status(500).send(e);
  // }
});

// GET ALL TASKS OF LOGGED IN USER; + FILTER
/*
GET ALL TEAMS
- filter members according to position

*/
router.get('/me', auth, async (req, res) => {
  const match = {};
  const sort = {};

  // console.log(req.query);

  if (req.query.isCompleted) {
    //add the field isCompleted to match objects
    match.isCompleted = req.query.isCompleted;
  }

  if (req.query.sortBy) {
    const parts = req.query.sortBy.split(':');
    // console.log(parts);
    console.log(parts[0]);
    console.log(parts[1]);
    sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;
  }
  // console.log(sort);

  try {
    //APPROACH 1
    // const tasks = await Task.find({ memberId: req.member._id });

    //APPROACH 2
    await req.member
      .populate({
        path: 'tasks',
        // HARD CODED value
        // match: { isCompleted: false }
        match,
        options: {
          limit: parseInt(req.query.limit),
          skip: parseInt(req.query.skip),
          sort
        }
      })
      .execPopulate();

    // if (!task) {
    //   return res.status(404).send({ message: 'No tasks found' });
    // }

    res.send(req.member.tasks);
  } catch (e) {
    res.status(500).send(e);
  }
});

// GET ALL BY ADMIN
router.get('/', auth, async (req, res) => {
  try {
    const tasks = await Task.find().populate({
      path: 'memberId',
      select: ['username', 'firstName']
    });

    //we shouldn't delete a member hence no need for not eqal or ne

    if (!tasks) {
      return res.status(404).send();
    }

    res.status(200).send(tasks);
  } catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
});

//GET ONE
router.get('/:id', auth, async (req, res) => {
  try {
    const task = await Task.findById(req.params.id).populate({
      path: 'memberId',
      select: ['username', 'firstName']
    });

    //we shouldn't delete a member hence no need for not eqal or ne

    if (!task) {
      return res.status(404).send();
    }

    res.status(200).send(task);
  } catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
});

//UPDATE ONE OF YOUR TASKS
router.patch('/:id', auth, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ['description', 'isCompleted'];

  const isValidUpdate = updates.every(update =>
    allowedUpdates.includes(update)
  );

  if (!isValidUpdate) {
    return res.status(400).send({ error: 'Update is not allowed.' });
  }

  try {
    const task = await Task.findOne({
      _id: req.params.id,
      memberId: req.member._id
    });

    if (!task) {
      return res.status(404).save();
    }

    updates.forEach(update => (task[update] = req.body[update]));
    await task.save();
    res.send(task);
  } catch (e) {
    res.status(500).send(e);
  }
});

//DELETE ONE OF YOUR TASKS
router.delete('/:id', auth, async (req, res) => {
  try {
    const task = await Task.findOneAndDelete({
      _id: req.params.id,
      memberId: req.member._id
    });
    if (!task) {
      //Not found
      return res.status(404).send(e);
    }

    res.send(task);
  } catch (e) {
    console.log('error');
    return res.status(500).send(e);
  }
});

module.exports = router;
