/*
PROMISES
- JS Objects
- Allows for asynchronous operations

SYNCHRONOUS
- executed in sequence
- each statement waits for the previous statement to finish before executing

ASYNCHRONOUS
- non-blocking
- asychronous operations execute without blocking other operations
- reduces waiting time for user
- always returns a promise

ASYNC 
- placed before a function so that it returns a promise

AWAIT
- literally makes JS wait until a promise settles and returns the result
- used only inside an async function
- makes asynchronous behave like synchronous because it blocks code execution


ASYNC AWAIT
- more elegant syntax for getting promise result

*/

//group per resource
//Declare Models
const Team = require('../models/teams');
const express = require('express');
//kasi wala dito si app so we need the router to handle routing
//Routing refers to determining how an application responds to a client request to a particular endpoint
const router = express.Router();

//Routes/Endpoints
//1 CREATE
router.post('/', async (req, res) => {
  // return res.send(req.body);
  const team = new Team(req.body);

  //save
  // team
  //   .save()
  //   .then(() => {
  //     res.send(team);
  //   })
  //   .catch(e => {
  //     // bad request
  //     // HTTP response status codes
  //     res.status(400).send(e);
  //   });

  try {
    await team.save();
    res.send(team);
  } catch (e) {
    res.status(400).send(e);
  }
});
//2 GET ALL
router.get('/', async (req, res) => {
  // return res.send("get all teams");

  // Team.find()
  //   .then(teams => {
  //     //ok
  //     res.status(200).send(teams);
  //   })
  //   .catch(e => {
  //     //internal server error
  //     res.status(500).send(e);
  //   });

  try {
    const teams = await Team.find();
    res.status(200).send(teams);
  } catch (e) {
    res.status(500).send(e);
  }
});

//3 GET ONE
router.get('/:id', async (req, res) => {
  // return res.send("get one");

  // console.log(req.params);
  // console.log(req.params.id);
  const _id = req.params.id;

  //Mongoose Models Query
  //Unlike Mongodb native driver, with Mongoose automatically converted string ids into objectID
  // Team.findById(_id)
  //   .then(team => {
  //     if (!team) {
  //       //Not found
  //       console.log('not found');
  //       return res.status(404).send(e);
  //     }

  //     res.send(team);
  //   })
  //   .catch(e => {
  //     //server error
  //     console.log('server error');
  //     return res.status(500).send(e);
  //   });

  try {
    const team = await Team.findById(_id);
    if (!team) {
      return res.status(400).send();
    }
    res.send(team);
  } catch (e) {
    res.status(500).send(e);
  }
});

//4 UPDATE
router.patch('/:id', async (req, res) => {
  // return error 400 when you try to edit property that doesn't exist
  const updates = Object.keys(req.body);
  const allowedUpdates = ['name', 'budget', 'level'];
  //map() returns a new Array of objects created by taking some action on the original item. ... every() returns a boolean
  const isValidUpdate = updates.every(update =>
    allowedUpdates.includes(update)
  );
  // console.log(!isValidUpdate);
  if (!isValidUpdate) {
    return res.status(400).send({ error: 'Invalid Updates!' });
  }
  // return res.send("update one");
  // const _id = req.params.id;
  // // console.log(_id);
  // // res.send(req.body)
  // Team.findByIdAndUpdate(_id, req.body, { new: true })
  //   .then(team => {
  //     if (!team) {
  //       //Not found
  //       console.log('not found');
  //       return res.status(404).send(e);
  //     }
  //     res.send(team);
  //   })
  //   .catch(e => {
  //     //server error
  //     console.log('server error');
  //     return res.status(500).send(e);
  //   });

  try {
    const team = await Team.findByIdAndUpdate(_id, req.body, {
      new: true,
      runValidators: true
    });

    // console.log(team);
    //Certain mongoose queries bypass more advanced features like middleware. To use them consistently
    // const team = await Team.findById(req.params.id);
    // updates.map(update => (team[update] = req.body[update]));
    // await team.save();

    if (!team) {
      return res.status(404).send();
    }

    res.send(team);
  } catch (e) {
    console.log('error');
    res.status(500).send(e);
  }
});

//5 DELETE
router.delete('/:id', async (req, res) => {
  // return res.send("delete one");

  const _id = req.params.id;

  // Team.findByIdAndDelete(_id)
  //   .then(team => {
  //     if (!team) {
  //       //Not found
  //       console.log('not found');
  //       return res.status(404).send(e);
  //     }
  //     res.send(team);
  //   })
  //   .catch(() => {
  //     return res.status(500).send(e);
  //   });

  try {
    const team = await Team.findByIdAndDelete(_id);
    if (!team) {
      //Not found
      console.log('not found');
      return res.status(404).send();
    } else {
      console.log(' found');
      res.send(team);
    }
  } catch (e) {
    console.log('error');
    return res.status(500).send(e);
  }
});

module.exports = router;
