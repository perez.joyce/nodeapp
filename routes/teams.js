/*
PROMISES
- JS Objects
- Allows for asynchronous operations

SYNCHRONOUS
- executed in sequence
- each statement waits for the previous statement to finish before executing

ASYNCHRONOUS
- non-blocking
- asychronous operations execute without blocking other operations
- reduces waiting time for user
- always returns a promise

ASYNC 
- placed before a function so that it returns a promise

AWAIT
- literally makes JS wait until a promise settles and returns the result
- used only inside an async function
- makes asynchronous behave like synchronous because it blocks code execution


ASYNC AWAIT
- more elegant syntax for getting promise result

=============================

JSON WEB TOKENS
https://www.npmjs.com/package/jsonwebtoken
Every single route we create will either be:
public and accessible to everyone (i.e., only signup and login routes will be made public)
private. require authentication.

JWTs (JSON Web Tokens) 
provide a nice system for issuing and validating authentication tokens. The authentication token will ensure that the client doesn’t need to log in every time they want to perform an operation on the server

Install the library: npm i jsonwebtoken

Use sign method to generate new token. 
1) It accepts 3 arguments:
data to embed in the token: This needs to include a unique identifier for the user. 
2) a secret phrase: This is used to issue and validate tokens, ensuring that the token data hasn’t been tampered with. 
3) a set of options: The example below uses expiresIn to create a token that’s valid for seven days. 

The server can verify the token using verify.
This requires two arguments:
1) the token to validate. 
2) the secret phrase that the token was created with. 

If valid, the embedded data will be returned. 
This would allow the server to figure out which user is performing the operation. 






*/

//group per resource
//Declare Models
const Team = require('../models/teams');
const express = require('express');
//kasi wala dito si app so we need the router to handle routing
//Routing refers to determining how an application responds to a client request to a particular endpoint
const router = express.Router();
const auth = require('../middleware/auth');
//Routes/Endpoints

//1 CREATE
router.post('/', auth, async (req, res) => {
  // return res.send(req.body);
  const team = new Team(req.body);

  // save
  // team
  //   .save()
  //   .then(() => {
  //     res.send(team);
  //   })
  //   .catch(e => {
  //     // bad request
  //     // HTTP response status codes
  //     res.status(400).send(e);
  //   });

  try {
    await team.save();
    res.send(team);
  } catch (e) {
    res.status(400).send(e);
  }
});

//2 GET ALL
router.get('/', auth, async (req, res) => {
  // return res.send("get all teams");

  // Team.find()
  //   .then(teams => {
  //     //ok
  //     res.status(200).send(teams);
  //   })
  //   .catch(e => {
  //     //internal server error
  //     res.status(500).send(e);
  //   });

  try {
    const teams = await Team.find();
    console.log(teams);
    res.status(200).send(teams);
  } catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
});

//3 GET ONE
router.get('/:id', async (req, res) => {
  // return res.send("get one");

  // console.log(req.params);
  // console.log(req.params.id);
  // const _id = req.params.id;

  //Mongoose Models Query
  //Unlike Mongodb native driver, with Mongoose automatically converted string ids into objectID
  // Team.findById(_id)
  //   .then(team => {
  //     if (!team) {
  //       //Not found
  //       console.log('not found');
  //       return res.status(404).send(e);
  //     }

  //     res.send(team);
  //   })
  //   .catch(e => {
  //     //server error
  //     console.log('server error');
  //     return res.status(500).send(e);
  //   });

  // const match = {};

  // if (req.query.) {
  //   //add the field isCompleted to match objects
  //   match.isCompleted = req.query.isCompleted === true;
  // }

  console.log(req.query.position);

  const match = {};

  if (req.query.position) {
    match.position = req.query.position;
  }

  try {
    const team = await Team.findById(req.params.id);

    if (!team) {
      //NOT FOUND
      return res.status(404).send({ message: 'Not found' });
    }

    if (req.query.members) {
      // await team.populate('members').execPopulate();
      await team.populate({ path: 'members', match }).execPopulate();
      return res.send(team.members);
    }

    res.send(team);
  } catch (e) {
    res.status(500).send(e);
  }
});

//4 UPDATE
router.patch('/:id', async (req, res) => {
  // return error 400 when you try to edit property that doesn't exist
  const updates = Object.keys(req.body);
  const allowedUpdates = ['name', 'budget', 'level'];
  //map() returns a new Array of objects created by taking some action on the original item. ... every() returns a boolean
  const isValidUpdate = updates.every(update =>
    allowedUpdates.includes(update)
  );
  // console.log(!isValidUpdate);
  if (!isValidUpdate) {
    return res.status(400).send({ error: 'Invalid Updates!' });
  }
  // return res.send("update one");
  const _id = req.params.id;
  // // console.log(_id);
  // // res.send(req.body)
  // Team.findByIdAndUpdate(_id, req.body, { new: true })
  //   .then(team => {
  //     if (!team) {
  //       //Not found
  //       console.log('not found');
  //       return res.status(404).send(e);
  //     }
  //     res.send(team);
  //   })
  //   .catch(e => {
  //     //server error
  //     console.log('server error');
  //     return res.status(500).send(e);
  //   });

  try {
    const team = await Team.findByIdAndUpdate(_id, req.body, {
      new: true,
      runValidators: true
    });

    console.log(team);

    if (!team) {
      return res.status(404).send(e);
    }

    res.send(team);
  } catch (e) {
    console.log('error');
    res.status(500).send(e);
  }
});

//5 DELETE
router.delete('/:id', async (req, res) => {
  // return res.send("delete one");

  const _id = req.params.id;

  // Team.findByIdAndDelete(_id)
  //   .then(team => {
  //     if (!team) {
  //       //Not found
  //       console.log('not found');
  //       return res.status(404).send(e);
  //     }
  //     res.send(team);
  //   })
  //   .catch(() => {
  //     return res.status(500).send(e);
  //   });

  try {
    const team = await Team.findByIdAndDelete(_id);
    if (!team) {
      //Not found
      console.log('not found');
      return res.status(404).send(e);
    } else {
      console.log(' found');
      res.send(team);
    }
  } catch (e) {
    console.log('error');
    return res.status(500).send(e);
  }
});

module.exports = router;

// GET ONE BOOKING THAT IS PAID
// GET THE USER OF THE SAID BOOKING, IF REQUESTED
router.get('/:id', async (req, res) => {
  const match = {};

  if (req.query.ispaid) {
    match.ispaid = req.ispaid;
  }

  try {
    const booking = Booking.findOne({ _id: req.params._id });

    if (!booking) {
      return res.status(404).send({ message: "Booking Doesn't Exist" });
    }

    //IF REQUEST CONTAINS USERS
    if (req.query.users) {
      await booking
        .populated({
          path: 'users',
          match
        })
        .execPopulate();

      res.send(booking.users);
    }

    res.send(booking);
  } catch (e) {
    res.status(500).send(e);
  }
});
