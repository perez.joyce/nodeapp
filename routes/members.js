const Member = require('../models/members');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const multer = require('multer');
const sharp = require('sharp');
const ObjectId = require('mongodb').ObjectID;

// CREATE
router.post('/', async (req, res) => {
  // try {
  //   res.send(req.body)
  // } catch (e) {
  //   console.log(e)
  // }

  const member = new Member(req.body);

  try {
    await member.save();

    res.send(member);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
});

//configure multer
const upload = multer({
  //destination: folder where uploads will be stored; automoatically createds the said folder
  // dest: 'images/members',
  limits: {
    fileSize: 1000000 //max file size limits in bytes
  },
  //cb: callback
  //idicates if file should be accepted
  fileFilter(req, file, cb) {
    //https://regex101.com/
    // \. --> escape special characters
    // () --> capture group
    // | --> or
    // $ --> anything that ends with the contents of the capture group
    if (!file.originalname.match(/\.(jpg|jpeg|png|PNG)$/)) {
      //call back
      return cb(new Error('Please upload an image'));
    }
    //call back
    cb(undefined, true);
  }
});

//UPLOAD IMAGE
//endpoint where client will be able to upload files
router.post('/upload', upload.single('upload'), auth, async (req, res) => {
  console.log(req.file);
  try {
    const buffer = await sharp(req.file.buffer)
      .resize({
        width: 500,
        height: 500
      })
      .png()
      .toBuffer();
    // req.member.profilePic = req.file.buffer;
    req.member.profilePic = buffer;
    await req.member.save();
    res.send(req.member);
  } catch (e) {
    // BAD REQUEST: Request is incorrect or corrupted and server can't understand it
    res.status(400).send({ error: e.message });
  }
});

//DELETE IMAGE
router.delete('/upload', auth, async (req, res) => {
  try {
    req.member.profilePic = undefined;
    await req.member.save();
    res.send(req.member);
  } catch (e) {
    res.status(400).send({ error: e.message });
  }
});

// GET ALL MEMBERS
router.get('/', auth, async (req, res) => {
  console.log(req.query);

  let where = {};

  if (req.query.isActive) {
    where = req.query;
  }

  try {
    const members = await Member.find(where).populate({
      path: 'teamId',
      select: 'name'
    });

    // console.log(members);
    res.status(200).send(members);
  } catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
});

// UPDATE OWN PROFILE
router.patch('/me', auth, async (req, res) => {
  console.log('patch');
  console.log(req.body);

  const updates = Object.keys(req.body);
  const allowedUpdates = [
    'firstName',
    'lastName',
    'position',
    'password',
    'teamId',
    'profilePic',
    'email',
    'username',
    'age'
  ];
  console.log(updates);
  //map() returns a new Array of objects created by taking some action on the original item. ... every() returns a boolean
  const isValidUpdate = updates.every(update =>
    allowedUpdates.includes(update)
  );
  // console.log(!isValidUpdate);
  if (!isValidUpdate) {
    return res.status(400).send({ error: 'Update is not allowed.' });
  }

  try {
    // const member = await Member.findByIdAndUpdate(req.params.id, req.body, {
    //   new: true,
    //   runValidators: true
    // });

    //Certain mongoose queries bypass more advanced features like middleware. To use them consistently
    // const member = await Member.findById(req.params.id);
    updates.map(update => (req.member[update] = req.body[update]));
    await req.member.save();

    //REFACTOR update routes for teams and tasks for consistency

    // if (!member) {
    //   return res.status(404).send();
    // }

    res.send(req.member);
  } catch (e) {
    console.log('error');
    res.status(500).send(e);
  }
});

// UPDATE OTHER PROFILE: ADMIN ONLY
router.patch('/:id', auth, async (req, res) => {
  console.log('patch');
  const updates = Object.keys(req.body);
  const allowedUpdates = ['position', 'teamId'];
  //map() returns a new Array of objects created by taking some action on the original item. ... every() returns a boolean
  const isValidUpdate = updates.every(update =>
    allowedUpdates.includes(update)
  );
  // console.log(!isValidUpdate);
  if (!isValidUpdate) {
    return res.status(400).send({ error: 'Update is not allowed.' });
  }

  try {
    const member = await Member.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });

    res.send(member);
  } catch (e) {
    console.log('error');
    res.status(500).send(e);
  }
});

// LOGIN
router.post('/login', async (req, res) => {
  console.log('login');
  //findByCredentials is a method that you defined in user.js (model). This is not from Mongoose.
  try {
    // console.log(req.body);
    const member = await Member.findByCredentials(
      req.body.email,
      req.body.password
    );
    // console.log(member);

    const token = await member.generateAuthToken();
    // console.log(token);
    // res.send(member);
    res.send({ member, token });
  } catch (e) {
    // console.log('not found');
    console.log(e);
    res.status(404).send(e);
  }
});

// GET OWN PROFILE
router.get('/me', auth, async (req, res) => {
  console.log('get own profile');
  res.send(req.member);
});

// LOGOUT ALL (ACTIVITY)
router.post('/logoutAll', auth, async (req, res) => {
  try {
    console.log('======req.member======');
    console.log(req.member);
    console.log('======req.member.tokens======');
    console.log(req.member.tokens);
    console.log('======req.tokens======');
    console.log(req.token);
    /*
        CLUES:
        Carefully study the results in your console. 
        You are logged out if req.members.tokens returns an empty array.
        Don't forget to save changes!
        Send back the message, "You've been successfully logged out of all devices!"
      */
    req.member.tokens = [];
    await req.member.save();
    res.send("You've been logged out of all devices");
  } catch (e) {
    res.status(500).send(e);
  }
});

// LOGOUT (MINI-CAPSTONE)
router.post('/logout', auth, async (req, res) => {
  try {
    // console.log(req.member);
    // console.log(req.member.tokens)
    // console.log(req.token)
    /* 
      Study the result in your console. 
      Create a contant variable called remainingTokens. Its value should be a method that filters a member's token so that it doesn't include the req's token.
      Assign the result of remainingTokens to the member's token.
      Await saving of changes made to member.
      Send back the member
    */
    // const remainingTokens = req.member.tokens.filter(token =>
    //   console.log(token)
    // );
    const remainingTokens = req.member.tokens.filter(
      token => token.token !== req.token
    );
    // console.log(remainingTokens);
    req.member.tokens = remainingTokens;
    await req.member.save();
    res.send(req.member);
  } catch (e) {
    res.status(500).send(e);
  }
});

// DELETE SELF (ACTIVITY)
router.delete('/me', auth, async (req, res) => {
  console.log('delete me');
  try {
    await req.member.remove();
    res.send(req.member);
  } catch (e) {
    res.send(500).send();
  }
});

//DELETE MEMBER
router.delete('/:id', auth, async (req, res) => {
  // console.log('delete one');
  // try {
  //   const member = await Member.findByIdAndDelete(req.params.id);
  //   res.send(member);
  // } catch (e) {
  //   res.send(500).send(e);
  // }

  try {
    const member = await Member.findByIdAndUpdate(
      req.params.id,
      { isActive: false },
      {
        new: true
      }
    );

    console.log(member);
    return res.send(member);
  } catch (error) {
    res.send(500).send(e);
  }
});

/*
Set up Postman environment variable (i.e., url)
Set up Postman authentication
- inherit from parent
- no auth

Hide Private data
- password
- tokens

Answer logout single device
Authentication vs Authorization

AUTHENTICATION
process of verifying oneself

AUTHORIZATION
process of verifying what you have access to.


Get profile when authenticated

Authenticate member endpoints
- refactor update route so that user can only update his/her account
- ACTIVITY: refactor delete route so that authenticated user can only delete his/her account and not of others

The Member-Task Relationship
- add memberId to Task Model
- add virtual to memberSchema
- set up tasks route (POST new task)
- set up tasks resource
- set up tasks route (GET all tasks of logged in user)

Authenticate task endpoints
- patch
- deletes

Activity: Apply above concepts to Member-Team Relationship so that
- you can get all members of a team using populate


*/

// // GET ALL USERS
// router.post('/', auth, (req, res) => {
//   try {
//     const users = await User.find({});

//     if(!users) {
//       res.status(404).send(e)
//     }

//     res.status(200).send(users);
//   } catch (error) {
//     res.status(500).send({ error: e.message });
//   }
// });

//GET A MEMBER
router.get('/:id', auth, async (req, res) => {
  console.log('get one');
  try {
    const member = await Member.findById(req.params.id).populate({
      path: 'teamId',
      select: 'name'
    });

    if (!member) {
      return res.status(404).send();
    }

    res.send(member);
  } catch (e) {
    res.status(500).send(e);
  }
});

//GET IMAGE
router.get('/:id/upload', async (req, res) => {
  console.log('get one inage');
  try {
    const member = await Member.findById(req.params.id);

    if (!member || !member.profilePic) {
      throw new Error();
    }

    //send back the correct data
    //tell client what type of data they'll get back
    res.set('Content-Type', 'image/png');
    res.send(member.profilePic);

    //delete profilePic from json object when member is returned
  } catch (e) {
    res.status(404).send(e);
  }
});

//GET A RANDOM MEMBER WITH A GIVEN TEAMID
//http://localhost:4000/members/team/:teamid
router.get('/team/:teamid', async (req, res) => {
  // console.log(ObjectId(req.params.teamid))
  // res.send(typeof ObjectId(req.params.teamid))
  try {
    const member = await Member.aggregate([
      { $match: { teamId: ObjectId(req.params.teamid) } },
      { $sample: { size: 1 } }
    ]);
    res.send(member);
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
