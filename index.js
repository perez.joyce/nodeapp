//Declare dependencies
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const multer = require('multer');
let cors = require('cors');
const connectDB = require("./config/db")

//Connect to local database
// mongoose.connect('mongodb://localhost:27017/mern_tracker', {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
//   useFindAndModify: false,
//   useCreateIndex: true
// });
// mongoose.connection.once('open', () => {
//   console.log('Now connected to local MongoDB server');
// });


//Connect to remote database
connectDB()

// app.get("/", (req, res) => res.send("API is running"))

//APPLY MIDDLEWARE
//Pipe analogy; request-response pipeline
//Parse incoming JSON to an object
app.use(express.json());
//Same purpose but for data coming from forms
app.use(express.urlencoded({ extended: true }));
//Cross-origin resource sharing (CORS) - nodejs middleware that allows backend and frontend to communicate
//npm install cors


app.use(cors());

//INCLUDE ROUTES TO THE REQUEST-RESPONSE PIPELINE
//RESOURCE - group of routes for a particular Model/Entity
///teams --> route
//get, post, delete, patch --> HTTP verbs/methods
//request --> the manner of asking something from your server
const teamsRoute = require('./routes/teams');
app.use('/teams', teamsRoute);

const membersRoute = require('./routes/members');
app.use('/members', membersRoute);

const tasksRoute = require('./routes/tasks');
app.use('/tasks', tasksRoute);

// //Declare Models
// const Team = require("./models/teams");

// //Routes/Endpoints
// //1 CREATE
// app.post('/teams', (req, res) => {
// 	// return res.send(req.body);
// 	const team = new Team(req.body);

// 	//save
// 	team.save()
// 		.then(() => {
// 			res.send(team)
// 		}).catch((e) => {
// 			// bad request
// 			// HTTP response status codes
// 			res.status(400).send(e)
// 		})
// })
// //2 GET ALL
// app.get('/teams', (req, res) => {
// 	// return res.send("get all teams");

// 	Team.find()
// 		.then((teams) => {
// 			//ok
// 			res.status(200).send(teams);
// 		}).catch((e) => {
// 			//internal server error
// 			res.status(500).send(e)
// 		})
// })
// //3 GET ONE
// app.get('/teams/:id', (req, res) => {
// 	// return res.send("get one");

// 	console.log(req.params)
// 	console.log(req.params.id)
// 	const _id = req.params.id;

// 	//Mongoose Models Query
// 	//Unlike Mongodb native driver, with Mongoose automatically converted string ids into objectID
// 	Team.findById(_id)
// 		.then((team) => {
// 			if(!team) {
// 				//Not found
// 				console.log("not found")
// 				return res.status(404).send(e);
// 			}

// 			res.send(team)
// 		}).catch((e) => {
// 			//server error
// 			console.log("server error")
// 			return res.status(500).send(e)
// 		})
// })
// //4 UPDATE
// app.patch('/teams/:id', (req, res) => {
// 	// return res.send("update one");

// 	const _id = req.params.id;
// 	// console.log(_id);
// 	// res.send(req.body)
// 	Team.findByIdAndUpdate(_id, req.body, {new: true})
// 		.then((team) => {
// 			if(!team) {
// 				//Not found
// 				console.log("not found")
// 				return res.status(404).send(e);
// 			}

// 			res.send(team)
// 		}).catch((e) => {
// 			//server error
// 			console.log("server error")
// 			return res.status(500).send(e)
// 		})
// })
// //5 DELETE
// app.delete('/teams/:id', (req, res) => {
// 	// return res.send("delete one");

// 	const _id = req.params.id;

// 	Team.findByIdAndDelete(_id)
// 		.then((team) => {
// 			if(!team) {
// 				//Not found
// 				console.log("not found")
// 				return res.status(404).send(e);
// 			}
// 			res.send(team)
// 		}).catch(() => {
// 			return res.status(500).send(e);
// 		})
// })

//configure multer
const upload = multer({
  dest: 'images', //destination/folder where uploads where be stored
  limits: {
    fileSize: 1000000 //max file size limits in bytes
  },
  //cb: callback
  //idicates if file should be accepted
  fileFilter(req, file, cb) {
    //https://regex101.com/
    // \. --> escape special characters
    // () --> capture group
    // | --> or
    // $ --> anything that ends with the contents of the capture group
    if (!file.originalname.match(/\.(jpg|jpeg|png|PNG)$/)) {
      //call back
      return cb(new Error('Please upload an image'));
    }
    //call back
    cb(undefined, true);
  }
});

//endpoint where client will be able to upload files
app.post(
  '/upload',
  upload.single('upload'),
  (req, res) => {
    res.send({ message: 'Successfully uploaded image!' });
  },
  (error, req, res, next) => {
    //BAD REQUEST: Request is incorrect or corrupted and server can't understand it
    res.status(400).send({ error: error.message });
  }
);

/*
ACTIVITY 
PART 1: SET UP
1) enable client to upload a file of a member inside images/members folder (DEMONSTRATE)
2) enable client to upload a file of a team inside images/teams folder

PART 2: VALIDATION AND EXPRESS ERROR HANDLING
3) enable clients to upload a pdf or word document (i.e., doc, docx, pdf) of a task inside images/tasks folder
4) refactor 1-3 so that an error message is displayed when a client uploads the wrong type of file

PART 3: SAVE UPLOAD FILE TO DB
5) team
- add logo field to teamSchema
- refactor POST team logo route to save logo to db instead of to file system

6) task
- add uploadDoc field to taskSchema
- refactor POST task doc route to save doc to db instead of to file system

PART 4: DELETE
7) member
- create a delete profilePic route
- a profilePic is deleted if its value is undefined
- check db at robo3t if profilePic has been deleted


8) team
- create a delete logo route
- a logo is deleted if its value is undefined
- check db at robo3t if profilePic has been deleted


9) task
- create a delete uploadDoc route
- an uploadDoc is deleted if its value is undefined
- check db at robo3t if profilePic has been deleted


PART 5: GET
10) team
- create a get logo route
- use Postman to test if image is displayed 
- use browser to test if image is displayed

11) task
- create a get uploadDoc route
- use Postmand to test if doc is displayed
- use browser to test if doc is displayed

PART 6: AUTO-CROP AND FORMAT
12. Apply auto-crop and format to team and task


*/

//Initialize server; set up for Heroku
const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log('Now listening for requests on port ' + port);
});
