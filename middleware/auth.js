const jwt = require('jsonwebtoken');
const Member = require('../models/members');
const config = require("config")
const secret = config.get("secret")

const auth = async (req, res, next) => {
  try {
    //STEP 1: access value from header using req.header
    //STEP 2: get jwt out of the entire header value using replace
    const token = req.header('Authorization').replace('Bearer ', '');

    //STEP 3: make sure that the token is valid
    const decoded = jwt.verify(token, secret);

    //STEP 4: find the member in the db and
    //STEP 5: check token is part of the tokens array
    const member = await Member.findOne({
      _id: decoded._id,
      'tokens.token': token
    });

    //STEP 6: Check if member doesn't exist
    if (!member) {
      throw new Error("doesn't exist");
    }

    //STEP 7: Give rout handler access to the user that we fetched in db
    req.member = member;

    //STEP 8: Make route handler run if user is authenticated

    //STEP 9: Give route handler to the token to be deleted correctly
    req.token = token;

    next();
  } catch (e) {
    //UNATHORIZED
    res.status(401).send({ error: 'Please authenticate' });
  }
};

module.exports = auth;
