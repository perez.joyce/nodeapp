/*
Description:
- String
- required
- maxlength is 100

TeamId
- String
- required

isCompleted
- Boolean
- default

*/

//Declare dependencies
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Define your schema
const taskSchema = new Schema(
  {
    description: {
      type: String,
      required: true,
      maxlength: 100
    },
    isCompleted: {
      type: Boolean,
      default: false
    },
    memberId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Member'
    }
  },
  {
    timestamps: true
  }
);

//Export your model
module.exports = mongoose.model('Task', taskSchema);
