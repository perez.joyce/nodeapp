//Declare dependencies
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');

//Define your schema
//Data Validation and Sanitation
/*Validation
RESTRICT what data can be stored in the database (e.g., type, required)

Sanitization
store data in a uniform and standardized FORMAT (e.g., trim, lowercase, change to boolean, )

Mongoose comes with support for basic validation and sanitization. 

required
used to validate that a value is provided for a given field

trim
used to remove extra spaces before or after data

lowercase
used to convert the data to lowercase before saving it to the database 
*/
// const teamSchema = new Schema(
// 	{
// 		name: String
// 	},
// 	{
// 		timestamps: true
// 	}
// );

const teamSchema = new Schema(
  {
    name: {
      type: String,
      // required: true,
      lowercase: true,
      trim: true
    }
    // isActive: {
    //   type: Boolean,
    //   required: true
    // }
    // budget: {
    //   type: Number,
    //   required: [true, 'Budget is required!'],
    //   validate(value) {
    //     if (value < 100) {
    //       //The throw statement throws a user-defined exception.
    //       throw new Error('Budget must be more than 100');
    //     }
    //   }
    // },
    // level: {
    //   type: Number,
    //   enum: [1, 2, 3],
    //   required: true
    // },
    // email: {
    //   type: String,
    //   required: true,
    //   validate(value) {
    //     //VALIDATION
    //     if (!validator.isEmail(value)) {
    //       console.log('invalid');
    //       throw new Error('Email is invalid');
    //     }

    //     //SANITATION
    //     return (this.email = validator.normalizeEmail(value, {
    //       all_lowercase: true
    //     }));
    //   }
    // }
  },
  {
    timestamps: true
  }
);

// teamSchema.virtual('members', {
//   ref: 'Member',
//   localField: '_id', //team._id
//   foreignField: 'teamId' //name of fiend on the other Model
// });

//set relationship between member and task

//Export your model
module.exports = mongoose.model('Team', teamSchema);
