/*
firstName
- String
- trim
- maxlength is 30
- default is J

lastName
- String
- trim
- maxlength is 30
- default is Doe

username
- String
- required
- trim
- maxlength is 10
- unique
- use validator to check if string contains only letters and numbers

position
- String
- enum - instructor, student, hr, admin, ca
- required

age
- Number
- min is 18
- required

password
- String
- required
- minlength is 5

email
- String
- required
- unique
- valid


*/
//Declare dependencies
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Task = require('./tasks');

const memberSchema = new Schema(
  {
    firstName: {
      type: String,
      trim: true,
      maxlength: 30,
      default: 'J'
    },
    lastName: {
      type: String,
      trim: true,
      maxlength: 30,
      default: 'Doe'
    },
    username: {
      type: String,
      required: true,
      trim: true,
      maxlength: 30,
      unique: true,
      validate(value) {
        //https://www.npmjs.com/package/validator
        if (!validator.isAlphanumeric(value)) {
          throw new Error('Username must contain alphanumeric characters only');
        }
      }
    },
    position: {
      type: String,
      enum: ['instructor', 'student', 'hr', 'admin', 'ca'],
      // required: true,
      default: 'student'
    },
    age: {
      type: Number,
      min: [18, 'You must be at least 18 years old to join a team!'],
      // required: true,
      default: 18
    },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      validate(value) {
        //VALIDATION
        if (!validator.isEmail(value)) {
          console.log('invalid');
          throw new Error('Email is invalid');
        }
      }
    },
    password: {
      type: String,
      required: true,
      minlength: 5
    },
    teamId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Team'
      // type: String
    },
    isActive: {
      type: Boolean,
      default: true
    },
    /*
    The file system gets wiped every time that you deploy causing you to lose data and user images.  So instead of storing them in the file system, we need to add a field onto the User model to store the image binary data. 

    Remove dest to keep the images from being saved in the avatars directory instead of the user’s profile. 

    */
    profilePic: {
      type: Buffer,
      default: undefined
    },
    //The following revision to userSchema tracks the tokens that we generate for users. This allows users to login from multiple devices and be logged out in one but still be logged in on others.
    tokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ]
  },
  {
    timestamps: true
  }
);

//Mongoose' pre middleware functions are executed one after another, when each middleware calls next.
memberSchema.pre('save', async function (next) {
  //acces the user you want to save
  //kapag nag arrow function di gagana si this
  const member = this;

  //Mongoose' isModified returns true if this document was modified, else false
  if (member.isModified('password')) {
    //salt contains number of rounds so bcrypt.hash(Sync) function knows how many rounds it has to do.
    //not genSaltSync and hashSync because we're already using await
    const salt = await bcrypt.genSalt(10);
    member.password = await bcrypt.hash(member.password, salt);
  }
  next();
});

//Delete member's tasks when member is removed
memberSchema.pre('remove', async function (next) {
  const member = this;
  await Task.deleteMany({ memberId: member._id });
  next();
});

// memberSchema.pre('remove', async function(next) {
//   const member = this;

//   //anything you want to execute before mag remove
// });

//why here? because this won't execute unless findByCredentials is called. at the moment, this applies to login
memberSchema.statics.findByCredentials = async function (email, password) {
  //find member by email

  const member = await this.findOne({ email });
  // console.log(member);

  if (!member) {
    throw new Error('Email is wrong'); //Failed to login
  }

  const isMatch = await bcrypt.compare(password, member.password);
  console.log(isMatch);
  if (!isMatch) {
    throw new Error("Password doesn't match"); //Failed to login
  }

  return member;
};

//generate auth token
//instance methos are accessible on the instances
memberSchema.methods.generateAuthToken = async function () {
  const member = this;
  /*
  Use sign method to generate new token. 
1) It accepts 3 arguments:
data to embed in the token: This needs to include a unique identifier for the user. 
2) a secret phrase: This is used to issue and validate tokens, ensuring that the token data hasn’t been tampered with. 
3) a set of options: The example below uses expiresIn to create a token that’s valid for seven days. 

The server can verify the token using verify.
This requires two arguments:
1) the token to validate. 
2) the secret phrase that the token was created with. 

If valid, the embedded data will be returned. 
This would allow the server to figure out which user is performing the operation. 
  */
  const config = require("config")
  const secret = config.get("secret")
  const token = jwt.sign({ _id: member._id.toString() }, secret, {
    expiresIn: '2 days'
  });
  //save token to db
  //The concat() method is used to merge two or more arrays
  member.tokens = member.tokens.concat({ token });
  await member.save();
  return token;
};

//hide private data
/*
Why did .toJSON work?
When we pass an object to res.send(), Express calls JSON.stringify() behind the scenes.

*/
memberSchema.methods.toJSON = function () {
  const member = this;

  //return RAW memberObject with all its field
  const memberObj = member.toObject();

  //delete private data
  delete memberObj.password;
  delete memberObj.tokens;
  delete memberObj.profilePic;

  return memberObj;
};

//set relationship between member and task
/*
Set up virtual property or relationship between user and tasks. 
Data is not stored in db but is set up for Mongoose to figure out who owns what and how these two things are related.

foreignField: name of the fiend on the other thing (i.e., task)

localField: where local data is stored 

*/
memberSchema.virtual('tasks', {
  ref: 'Task',
  localField: '_id', //member._id
  foreignField: 'memberId' //name of fiend on the other Model
});

// memberSchema.virtual('team', {
//   ref: 'Team',
//   localField: 'teamId', //member._id
//   foreignField: '_id' //name of fiend on the other Model
// });

memberSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Member', memberSchema);
